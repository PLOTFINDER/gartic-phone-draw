from PySide6 import QtCore
from PySide6.QtWidgets import QMainWindow

from ui_drawing import Ui_DrawingProcessWindow

class WindowDrawing(QMainWindow):
      def __init__(self):
        super(WindowDrawing, self).__init__()

        # ici on associe le .ui modélisé avec le designer
        self.drawingWindow = Ui_DrawingProcessWindow()
        self.drawingWindow.setupUi(self)


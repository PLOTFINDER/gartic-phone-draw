# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'imageWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_imageWindow(object):
    def setupUi(self, imageWindow):
        if not imageWindow.objectName():
            imageWindow.setObjectName(u"imageWindow")
        imageWindow.resize(300, 300)
        imageWindow.setMinimumSize(QSize(300, 300))
        imageWindow.setMaximumSize(QSize(300, 300))
        self.gridLayoutWidget = QWidget(imageWindow)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 10, 281, 92))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 4, 1, 1, 1)

        self.selectImageButton = QPushButton(self.gridLayoutWidget)
        self.selectImageButton.setObjectName(u"selectImageButton")

        self.gridLayout.addWidget(self.selectImageButton, 0, 1, 1, 1)

        self.imageUrlField = QLineEdit(self.gridLayoutWidget)
        self.imageUrlField.setObjectName(u"imageUrlField")
        self.imageUrlField.setClearButtonEnabled(True)

        self.gridLayout.addWidget(self.imageUrlField, 0, 3, 1, 1)

        self.importImageButton = QPushButton(self.gridLayoutWidget)
        self.importImageButton.setObjectName(u"importImageButton")

        self.gridLayout.addWidget(self.importImageButton, 0, 2, 1, 1)

        self.showImageButton = QPushButton(self.gridLayoutWidget)
        self.showImageButton.setObjectName(u"showImageButton")

        self.gridLayout.addWidget(self.showImageButton, 2, 1, 1, 3)

        self.imageProgressBar = QProgressBar(self.gridLayoutWidget)
        self.imageProgressBar.setObjectName(u"imageProgressBar")
        self.imageProgressBar.setEnabled(True)
        self.imageProgressBar.setValue(0)

        self.gridLayout.addWidget(self.imageProgressBar, 3, 1, 1, 3)

        self.gridLayoutWidget_2 = QWidget(imageWindow)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(90, 110, 201, 81))
        self.gridLayout_2 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.accSpinBox = QSpinBox(self.gridLayoutWidget_2)
        self.accSpinBox.setObjectName(u"accSpinBox")
        self.accSpinBox.setMinimum(1)
        self.accSpinBox.setMaximum(100)
        self.accSpinBox.setValue(100)

        self.gridLayout_2.addWidget(self.accSpinBox, 0, 1, 1, 1)

        self.colorsSlider = QSlider(self.gridLayoutWidget_2)
        self.colorsSlider.setObjectName(u"colorsSlider")
        self.colorsSlider.setEnabled(False)
        self.colorsSlider.setMinimum(1)
        self.colorsSlider.setMaximum(100)
        self.colorsSlider.setValue(100)
        self.colorsSlider.setOrientation(Qt.Horizontal)

        self.gridLayout_2.addWidget(self.colorsSlider, 1, 2, 1, 1)

        self.colorsSpinBox = QSpinBox(self.gridLayoutWidget_2)
        self.colorsSpinBox.setObjectName(u"colorsSpinBox")
        self.colorsSpinBox.setEnabled(False)
        self.colorsSpinBox.setMinimum(1)
        self.colorsSpinBox.setMaximum(100)
        self.colorsSpinBox.setValue(100)

        self.gridLayout_2.addWidget(self.colorsSpinBox, 1, 1, 1, 1)

        self.colorsCheckBox = QCheckBox(self.gridLayoutWidget_2)
        self.colorsCheckBox.setObjectName(u"colorsCheckBox")
        self.colorsCheckBox.setTristate(False)

        self.gridLayout_2.addWidget(self.colorsCheckBox, 1, 0, 1, 1)

        self.accuButton = QPushButton(self.gridLayoutWidget_2)
        self.accuButton.setObjectName(u"accuButton")

        self.gridLayout_2.addWidget(self.accuButton, 0, 3, 1, 1)

        self.colorsButton = QPushButton(self.gridLayoutWidget_2)
        self.colorsButton.setObjectName(u"colorsButton")
        self.colorsButton.setEnabled(False)

        self.gridLayout_2.addWidget(self.colorsButton, 1, 3, 1, 1)

        self.accuSlider = QSlider(self.gridLayoutWidget_2)
        self.accuSlider.setObjectName(u"accuSlider")
        self.accuSlider.setMinimum(1)
        self.accuSlider.setMaximum(100)
        self.accuSlider.setValue(100)
        self.accuSlider.setOrientation(Qt.Horizontal)

        self.gridLayout_2.addWidget(self.accuSlider, 0, 2, 1, 1)

        self.gridLayoutWidget_3 = QWidget(imageWindow)
        self.gridLayoutWidget_3.setObjectName(u"gridLayoutWidget_3")
        self.gridLayoutWidget_3.setGeometry(QRect(10, 110, 81, 151))
        self.gridLayout_3 = QGridLayout(self.gridLayoutWidget_3)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.accuLabel = QLabel(self.gridLayoutWidget_3)
        self.accuLabel.setObjectName(u"accuLabel")
        self.accuLabel.setScaledContents(False)

        self.gridLayout_3.addWidget(self.accuLabel, 0, 0, 1, 1)

        self.filterLabel = QLabel(self.gridLayoutWidget_3)
        self.filterLabel.setObjectName(u"filterLabel")
        self.filterLabel.setScaledContents(False)

        self.gridLayout_3.addWidget(self.filterLabel, 3, 0, 1, 1)

        self.speedLabel = QLabel(self.gridLayoutWidget_3)
        self.speedLabel.setObjectName(u"speedLabel")
        self.speedLabel.setScaledContents(False)

        self.gridLayout_3.addWidget(self.speedLabel, 2, 0, 1, 1)

        self.colorsLabel = QLabel(self.gridLayoutWidget_3)
        self.colorsLabel.setObjectName(u"colorsLabel")
        self.colorsLabel.setScaledContents(False)

        self.gridLayout_3.addWidget(self.colorsLabel, 1, 0, 1, 1)

        self.gridLayoutWidget_4 = QWidget(imageWindow)
        self.gridLayoutWidget_4.setObjectName(u"gridLayoutWidget_4")
        self.gridLayoutWidget_4.setGeometry(QRect(90, 190, 201, 31))
        self.gridLayout_4 = QGridLayout(self.gridLayoutWidget_4)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.speedSpinBox = QDoubleSpinBox(self.gridLayoutWidget_4)
        self.speedSpinBox.setObjectName(u"speedSpinBox")
        self.speedSpinBox.setDecimals(4)
        self.speedSpinBox.setMaximum(2.000000000000000)
        self.speedSpinBox.setSingleStep(0.001000000000000)
        self.speedSpinBox.setValue(0.100000000000000)

        self.gridLayout_4.addWidget(self.speedSpinBox, 0, 0, 1, 1)

        self.messageLabel = QLabel(self.gridLayoutWidget_4)
        self.messageLabel.setObjectName(u"messageLabel")

        self.gridLayout_4.addWidget(self.messageLabel, 0, 1, 1, 1)

        self.gridLayoutWidget_5 = QWidget(imageWindow)
        self.gridLayoutWidget_5.setObjectName(u"gridLayoutWidget_5")
        self.gridLayoutWidget_5.setGeometry(QRect(90, 230, 201, 31))
        self.gridLayout_5 = QGridLayout(self.gridLayoutWidget_5)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.filterComboBox = QComboBox(self.gridLayoutWidget_5)
        self.filterComboBox.setObjectName(u"filterComboBox")

        self.gridLayout_5.addWidget(self.filterComboBox, 0, 0, 1, 1)

        self.gridLayoutWidget_6 = QWidget(imageWindow)
        self.gridLayoutWidget_6.setObjectName(u"gridLayoutWidget_6")
        self.gridLayoutWidget_6.setGeometry(QRect(60, 260, 161, 41))
        self.gridLayout_7 = QGridLayout(self.gridLayoutWidget_6)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(0, 0, 0, 0)
        self.exitImageButton = QPushButton(self.gridLayoutWidget_6)
        self.exitImageButton.setObjectName(u"exitImageButton")

        self.gridLayout_7.addWidget(self.exitImageButton, 0, 0, 1, 1)


        self.retranslateUi(imageWindow)

        QMetaObject.connectSlotsByName(imageWindow)
    # setupUi

    def retranslateUi(self, imageWindow):
        imageWindow.setWindowTitle(QCoreApplication.translate("imageWindow", u"Image", None))
        self.selectImageButton.setText(QCoreApplication.translate("imageWindow", u"Select image", None))
        self.imageUrlField.setInputMask("")
        self.imageUrlField.setText("")
        self.importImageButton.setText(QCoreApplication.translate("imageWindow", u"Import image", None))
        self.showImageButton.setText(QCoreApplication.translate("imageWindow", u"Show image", None))
        self.colorsCheckBox.setText("")
        self.accuButton.setText(QCoreApplication.translate("imageWindow", u"Apply", None))
        self.colorsButton.setText(QCoreApplication.translate("imageWindow", u"Apply", None))
        self.accuLabel.setText(QCoreApplication.translate("imageWindow", u"Accuracy", None))
        self.filterLabel.setText(QCoreApplication.translate("imageWindow", u"Filter", None))
        self.speedLabel.setText(QCoreApplication.translate("imageWindow", u"Speed (ms)", None))
        self.colorsLabel.setText(QCoreApplication.translate("imageWindow", u"% of colors", None))
        self.messageLabel.setText(QCoreApplication.translate("imageWindow", u"Delay between each click", None))
        self.exitImageButton.setText(QCoreApplication.translate("imageWindow", u"Exit", None))
    # retranslateUi


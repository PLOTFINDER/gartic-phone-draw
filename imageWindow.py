from PySide6.QtWidgets import QMainWindow

from ui_image import Ui_imageWindow

class WindowImage(QMainWindow):
      def __init__(self):
        super(WindowImage, self).__init__()

        # ici on associe le .ui modélisé avec le designer
        self.imageWindow = Ui_imageWindow()
        self.imageWindow.setupUi(self)

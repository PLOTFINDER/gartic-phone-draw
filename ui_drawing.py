# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'drawingWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_DrawingProcessWindow(object):
    def setupUi(self, DrawingProcessWindow):
        if not DrawingProcessWindow.objectName():
            DrawingProcessWindow.setObjectName(u"DrawingProcessWindow")
        DrawingProcessWindow.setWindowModality(Qt.NonModal)
        DrawingProcessWindow.resize(250, 60)
        DrawingProcessWindow.setWindowOpacity(1.000000000000000)
        DrawingProcessWindow.setAutoFillBackground(False)
        self.gridLayoutWidget = QWidget(DrawingProcessWindow)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 10, 121, 41))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.ImageDrawingProgressBar = QProgressBar(self.gridLayoutWidget)
        self.ImageDrawingProgressBar.setObjectName(u"ImageDrawingProgressBar")
        self.ImageDrawingProgressBar.setValue(0)
        self.ImageDrawingProgressBar.setInvertedAppearance(False)

        self.gridLayout.addWidget(self.ImageDrawingProgressBar, 0, 0, 1, 1)

        self.gridLayoutWidget_2 = QWidget(DrawingProcessWindow)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(140, 0, 101, 61))
        self.gridLayout_2 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.gridLayoutWidget_2)
        self.label.setObjectName(u"label")

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.label_2 = QLabel(self.gridLayoutWidget_2)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)


        self.retranslateUi(DrawingProcessWindow)

        QMetaObject.connectSlotsByName(DrawingProcessWindow)
    # setupUi

    def retranslateUi(self, DrawingProcessWindow):
        DrawingProcessWindow.setWindowTitle(QCoreApplication.translate("DrawingProcessWindow", u"Drawing", None))
        self.label.setText(QCoreApplication.translate("DrawingProcessWindow", u"Hold 'Esc' to stop", None))
        self.label_2.setText(QCoreApplication.translate("DrawingProcessWindow", u"Hold 'P' to pause", None))
    # retranslateUi


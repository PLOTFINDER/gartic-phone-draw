# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_QWindow(object):
    def setupUi(self, QWindow):
        if not QWindow.objectName():
            QWindow.setObjectName(u"QWindow")
        QWindow.resize(350, 100)
        QWindow.setMinimumSize(QSize(350, 100))
        QWindow.setMaximumSize(QSize(350, 100))
        self.layoutWidget = QWidget(QWindow)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(20, 10, 311, 81))
        self.gridLayout = QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.startButton = QPushButton(self.layoutWidget)
        self.startButton.setObjectName(u"startButton")

        self.verticalLayout.addWidget(self.startButton)

        self.mainWindowExit = QPushButton(self.layoutWidget)
        self.mainWindowExit.setObjectName(u"mainWindowExit")

        self.verticalLayout.addWidget(self.mainWindowExit)


        self.gridLayout.addLayout(self.verticalLayout, 0, 1, 2, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.selectImageMenuButton = QPushButton(self.layoutWidget)
        self.selectImageMenuButton.setObjectName(u"selectImageMenuButton")

        self.horizontalLayout.addWidget(self.selectImageMenuButton)


        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.settingsButton = QPushButton(self.layoutWidget)
        self.settingsButton.setObjectName(u"settingsButton")

        self.horizontalLayout_2.addWidget(self.settingsButton)


        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)

        QWidget.setTabOrder(self.startButton, self.settingsButton)
        QWidget.setTabOrder(self.settingsButton, self.selectImageMenuButton)
        QWidget.setTabOrder(self.selectImageMenuButton, self.mainWindowExit)

        self.retranslateUi(QWindow)

        QMetaObject.connectSlotsByName(QWindow)
    # setupUi

    def retranslateUi(self, QWindow):
        QWindow.setWindowTitle(QCoreApplication.translate("QWindow", u"Draw it", None))
        self.startButton.setText(QCoreApplication.translate("QWindow", u"Start", None))
        self.mainWindowExit.setText(QCoreApplication.translate("QWindow", u"Exit", None))
        self.selectImageMenuButton.setText(QCoreApplication.translate("QWindow", u"Image", None))
        self.settingsButton.setText(QCoreApplication.translate("QWindow", u"Settings", None))
    # retranslateUi


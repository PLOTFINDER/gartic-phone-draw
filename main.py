import io
import sys
import requests

from PySide6.QtWidgets import QApplication, QMainWindow
from PySide6 import QtCore


from ui_main import Ui_QWindow
from settings import WindowSettings
from imageWindow import WindowImage
from drawing import WindowDrawing

import time
from functions import *
import configparser
from image import *
import math
from functools import cache
# pyside6-uic ui/settingsWindow.ui > ui_settings.py
# pyside6-uic ui/mainWindow.ui > ui_main.py
# pyside6-uic ui/imageWindow.ui > ui_image.py
# pyside6-uic ui/drawingWindow.ui > ui_drawing.py




class MainWindow(QMainWindow):


    def __init__(self):
        # appel au constructeur de la classe Parent, ici QMainWindow
        super(MainWindow, self).__init__()

        # variables
        self.__tempSettings = configparser.ConfigParser()
        self.__tempSettings.add_section('settings')
        self.__settings = loadConfig()
        self.image = None
        self.imageNormal = None
        self.IUC = None
        self.delay = 0.1

        # main Window
        self.ui = Ui_QWindow()
        self.ui.setupUi(self)

        # Settings window
        self.uiSettings = WindowSettings()
        self.uiSettingsWindow = self.uiSettings.settingsWindow
        self.uiSettings.hide()

        # Image Window
        self.uiImage = WindowImage()
        self.uiImageWindow = self.uiImage.imageWindow
        self.uiImage.hide()

        # Drawing Window
        self.uiDrawing = WindowDrawing()
        self.uiDrawingWindow = self.uiDrawing.drawingWindow
        self.uiDrawing.hide()
        self.uiDrawing.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)

        # Connections
        self.ui.settingsButton.clicked.connect(self.settings)
        self.ui.selectImageMenuButton.clicked.connect(self.selectImageMenu)
        self.ui.startButton.clicked.connect(self.startDrawing)
        self.ui.mainWindowExit.clicked.connect(self.exitMain)

        self.uiSettingsWindow.drawingZoneButton.clicked.connect(self.drawingZone)
        self.uiSettingsWindow.settingsExitButton.clicked.connect(self.exitSettings)
        self.uiSettingsWindow.saveButton.clicked.connect(self.saveSettings)
        self.uiSettingsWindow.colorPickerButton.clicked.connect(self.colorPicker)
        self.uiSettingsWindow.rButton.clicked.connect(self.R_Color)
        self.uiSettingsWindow.gButton.clicked.connect(self.G_Color)
        self.uiSettingsWindow.bButton.clicked.connect(self.B_Color)
        self.uiSettingsWindow.finishButton.clicked.connect(self.finish)
        self.uiSettingsWindow.penButton.clicked.connect(self.pen)
        self.uiSettingsWindow.penSizeButton.clicked.connect(self.penSize)
        self.uiSettingsWindow.exportButton.clicked.connect(self.exportSettings)
        self.uiSettingsWindow.safeZoneButton.clicked.connect(self.safeZone)

        self.uiImageWindow.selectImageButton.clicked.connect(self.selectImage)
        self.uiImageWindow.exitImageButton.clicked.connect(self.exitImageMenu)
        self.uiImageWindow.importImageButton.clicked.connect(self.importImage)
        self.uiImageWindow.showImageButton.clicked.connect(self.showImage)
        self.uiImageWindow.imageProgressBar.hide()

        self.uiImageWindow.accuSlider.valueChanged.connect(self.accuSChanged)
        self.uiImageWindow.accSpinBox.valueChanged.connect(self.accuSBChanged)
        self.uiImageWindow.accuButton.clicked.connect(self.applyAccu)

        self.uiImageWindow.colorsSlider.valueChanged.connect(self.colorsSChanged)
        self.uiImageWindow.colorsSpinBox.valueChanged.connect(self.colorsSBChanged)
        self.uiImageWindow.colorsButton.clicked.connect(self.applyColors)
        self.uiImageWindow.colorsCheckBox.stateChanged.connect(self.colorsCheckBoxChecked)

        self.uiImageWindow.speedSpinBox.valueChanged.connect(self.clickDelayChanged)

        self.uiImageWindow.filterComboBox.hide()




    def clickDelayChanged(self):
        self.delay = self.uiImageWindow.speedSpinBox.value()

    def applyColors(self):
        if self.imageNormal is not None:
            self.imageToPixels(self.imageNormal.copy())

    def colorsCheckBoxChecked(self):
        if self.uiImageWindow.colorsCheckBox.isChecked():
            self.uiImageWindow.colorsButton.setEnabled(True)
            self.uiImageWindow.colorsSpinBox.setEnabled(True)
            self.uiImageWindow.colorsSlider.setEnabled(True)
        else:
            self.uiImageWindow.colorsButton.setEnabled(False)
            self.uiImageWindow.colorsSpinBox.setEnabled(False)
            self.uiImageWindow.colorsSlider.setEnabled(False)

    def colorsSChanged(self):
        pass

    def colorsSBChanged(self):
        pass

    def applyAccu(self):
        pass

    def accuSChanged(self):
        pass

    def accuSBChanged(self):
        pass

    def showImage(self):
        if self.uiImageWindow.imageUrlField.text() != "":
            self.imageNormal = imageFromURL(self.uiImageWindow.imageUrlField.text())
            self.findUniqueColors(self.imageNormal)
            self.imageToPixels(self.imageNormal)
            self.image.show()
        elif self.image is not None:
            self.image.show()


    def importImage(self):
        importImage()

    def exitImageMenu(self):
        self.show()
        self.uiImage.hide()

    def selectImageMenu(self):
        self.hide()
        self.uiImage.show()


    def exitMain(self):
        self.uiSettings.close()
        self.close()

    def exitSettings(self):
        self.show()
        self.uiSettings.hide()

    def selectImage(self):
        self.uiImage.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()
        time.sleep(1)
        coordB = None
        while coordB == None:
            coordB = oneSelection()

        image = grabImage(coordA[0],coordA[1],coordB[0],coordB[1])
        #image = image.quantize(16)
        self.imageNormal = image

        self.findUniqueColors(image)

        self.imageToPixels(image)

        self.uiImage.show()

    def findUniqueColors(self, image):
        unique_colors = set()
        for i in range(image.size[0]):
            for j in range(image.size[1]):
                pixel = image.getpixel((i, j))
                unique_colors.add(pixel)
        self.IUC = len(unique_colors)

    def imageToPixels(self, image):
        if self.uiImageWindow.colorsCheckBox.isChecked():
            accu = round(self.uiImageWindow.colorsSlider.value() * self.IUC / 100)
            accu = round(accu * 255 / self.IUC)
            #print(self.IUC , accu)
            image = convertImage(image, accu)

        self.image = image
        self.imageSize = self.image.size
        self.imageBand = list(self.image.getdata())
        self.listePixel = []

        self.uiImageWindow.imageProgressBar.show()
        self.uiImageWindow.imageProgressBar.setValue(0)


        for y in range(0, self.imageSize[1]):
            for x in range(0, self.imageSize[0]):
                pixel = self.imageBand[y * self.imageSize[0] + x]
                self.listePixel.append(Img(x, y, pixel))
                self.uiImageWindow.imageProgressBar.setValue(y * self.imageSize[0] + x * 100 / (len(self.imageBand) - 1))

        print(self.listePixel[0].getColor())
        self.uiImageWindow.imageProgressBar.setValue(0)
        self.listePixel.sort(key=lambda x: x.getColor(), reverse=True)
        self.uiImageWindow.imageProgressBar.setValue(100)
        print(len(self.listePixel))
        # self.listePixel = sortPixels(self.listePixel)
        # print(len(self.listePixel))

        self.uiImageWindow.imageProgressBar.setValue(0)
        unique_colors = set()
        for pixel in self.listePixel:
            unique_colors.add(pixel.getColor())

        self.uiImageWindow.imageProgressBar.setValue(100)

        self.uniqueColors = unique_colors
        # self.uiImageWindow.imageProgressBar.setValue(0)
        # self.listePixel = sortPixelsByColors(self.listePixel, self.uniqueColors)
        # self.uiImageWindow.imageProgressBar.setValue(100)

        self.uiImageWindow.imageProgressBar.hide()


    def settings(self):
        self.hide()
        self.uiSettings.show()

    def getConfigByName(self, name):
        res = self.__settings.get("settings", name)
        res = res.replace("(", "")
        res = res.replace(")", "")
        res = res.replace(",", "")
        res = res.split()
        res = [int(i) for i in res]

        return res

    def startDrawing(self):
        self.hide()
        # self.uiDrawing.show()
        self.pickPen()
        self.pickPenSize()
        pos = self.getConfigByName("drawingzone")
        startPos = pos[0:2]
        print(startPos)
        time.sleep(5)
        lPixel = None
        lPixelPos = None
        lineStart = None
        lineEnd = None

        for pixel in self.listePixel:
            color = pixel.getColor()
            r = color[0]
            g = color[1]
            b = color[2]

            pos = pixel.getPos()
            x = pos.x
            y = pos.y


            if lPixel != None:
                if lPixel != color:

                    if lineStart == lineEnd and lineStart != None:
                        # linePixels(startPos, lineStart, lineEnd)
                        writePixel(startPos, x, y)
                    else:
                        lineEnd = lPixelPos
                        time.sleep(self.delay)
                        linePixels(startPos,lineStart,lineEnd)
                        time.sleep(self.delay)
                        lineStart = pos

                    self.clickColorPicker()
                    time.sleep(0.1)
                    if lPixel[0] != r: self.pickR(r)
                    if lPixel[1] != g: self.pickG(g)
                    if lPixel[2] != b: self.pickB(b)
                    time.sleep(0.1)
                    self.clickSafeZone()

                else:
                    if lPixelPos.x+1 == pos.x and lPixelPos.y == pos.y:
                        pass
                    elif lPixelPos.y+1 == pos.y and lPixelPos.x == pos.x:
                        pass
                    else:
                        if lineStart == lineEnd and lineStart != None:
                            # linePixels(startPos, lineStart, lineEnd)
                            writePixel(startPos, x, y)
                        else:
                            lineEnd = lPixelPos
                            time.sleep(self.delay)
                            linePixels(startPos, lineStart, lineEnd)
                            time.sleep(self.delay)
                            lineStart = pos

            else:
                lineStart = pos
                self.clickColorPicker()
                time.sleep(0.1)
                self.pickR(r)
                self.pickG(g)
                self.pickB(b)
                self.clickSafeZone()
                time.sleep(0.1)


            lPixel = color
            lPixelPos = pos

            # if keyPressed("p") or keyPressed("P"):
            #     time.sleep(5)
            #     while not keyPressed("p") or not keyPressed("P"):
            #         print("paused")
            #         time.sleep(1)
            #     else:
            #         pass

            if interupt(): break

        # self.uiDrawing.hide()
        self.show()


    def clickSafeZone(self):
        pos = self.getConfigByName("safezone")
        click(pos[0], pos[1])


    def pickR(self, col):

        pos = self.getConfigByName("r")
        click(pos[0], pos[1])
        click(pos[0], pos[1])
        # pressKey("backspace")
        # pressKey("backspace")
        # pressKey("backspace")

        for i in str(col):
            pressKey(str(i))



    def pickG(self, col):

        pos = self.getConfigByName("g")
        click(pos[0], pos[1])
        click(pos[0], pos[1])
        # pressKey("backspace")
        # pressKey("backspace")
        # pressKey("backspace")

        for i in str(col):
            pressKey(str(i))

    def pickB(self, col):

        pos = self.getConfigByName("b")
        click(pos[0], pos[1])
        click(pos[0], pos[1])
        # pressKey("backspace")
        # pressKey("backspace")
        # pressKey("backspace")

        for i in str(col):
            pressKey(str(i))


    def pickPen(self):
        pos = self.getConfigByName("pen")
        click(pos[0],pos[1])

    def pickPenSize(self):
        pos = self.getConfigByName("pensize")
        click(pos[0], pos[1])


    def clickColorPicker(self):
        pos = self.getConfigByName("colorpicker")
        click(pos[0], pos[1])

    def drawingZone(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()
        time.sleep(1)
        coordB = None
        while coordB == None:
            coordB = oneSelection()

        self.uiSettingsWindow.drawingZoneButton.setText("Drawing Zone ✔")

        self.uiSettings.show()
        self.setTempSettings("drawingzone",str(((coordA.x,coordA.y),(coordB.x,coordB.y))))

    def setTempSettings(self,name,coor):
        self.__tempSettings.set("settings", name,coor)

    def safeZone(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.safeZoneButton.setText("Safe Zone ✔")

        self.uiSettings.show()
        self.setTempSettings("safezone", str((coordA.x,coordA.y)))

    def colorPicker(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.colorPickerButton.setText("Color Picker ✔")

        self.uiSettings.show()
        self.setTempSettings("colorpicker",str((coordA.x,coordA.y)))

    def R_Color(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.rButton.setText("R ✔")

        self.uiSettings.show()
        self.setTempSettings("r", str((coordA.x,coordA.y)))

    def G_Color(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.gButton.setText("G ✔")

        self.uiSettings.show()
        self.setTempSettings("g", str((coordA.x,coordA.y)))

    def B_Color(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.bButton.setText("B ✔")

        self.uiSettings.show()
        self.setTempSettings("b", str((coordA.x,coordA.y)))

    def pen(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.penButton.setText("Pen ✔")

        self.uiSettings.show()
        self.setTempSettings("pen", str((coordA.x,coordA.y)))

    def penSize(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.penSizeButton.setText("Pen Size ✔")

        self.uiSettings.show()
        self.setTempSettings("pensize", str((coordA.x,coordA.y)))

    def importSettings(self):
        pass

    def exportSettings(self):
        updateConfig(self.__settings)

    def saveSettings(self):
        self.uiSettingsWindow.saveButton.setText("Save ✔")
        self.__settings = self.__tempSettings


    def finish(self):
        self.uiSettings.hide()
        coordA = None
        while coordA == None:
            coordA = oneSelection()

        self.uiSettingsWindow.finishButton.setText("Finish Button ✔")

        self.uiSettings.show()
        self.setTempSettings("finish", str((coordA.x,coordA.y)))


## PROGRAMME PRINCIPAL ##
if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())


import io
import os

import PIL
import pynput
import keyboard
import pyautogui
import pyscreenshot as ImageGrab
import configparser
import subprocess


import requests
from PIL import Image
import time
from image import *

pyautogui.PAUSE = 0

def openExplorer(path):
    subprocess.Popen(r'explorer /select,"path"')

def importImage():
    path = os.path.realpath(__file__)
    openExplorer(path)

def imageFromURL(url):
    image_bytes = None
    try:
        response = requests.get(url)
        image_bytes = io.BytesIO(response.content)
    except Exception as e:
        raise e

    finally:
        img = None
        try:
            img = PIL.Image.open(image_bytes)
        except Exception as e:
            raise e

        finally:
            return img

def sortPixelsByColors(pixels, colors):

    listePixels = []

    for color in colors:
        listePixels.append([i for i in pixels if i.getColor() == color])

    listePixels.sort(key=len, reverse=True)

    listePixels = [item for sublist in listePixels for item in sublist]

    return listePixels
# def sortPixels(pixels):
#
#     lPixel = None
#     lPixelPos = None
#     lineStart = None
#     lineEnd = None
#     newPixels = []
#
#     line = []
#     lines = []
#     alone = []
#
#     for index in range(len(pixels)):
#         color = pixels[index].getColor()
#         pos = pixels[index].getPos()
#
#
#         if lPixel != None:
#             if lPixel != color:
#
#                 newPixels.extend(lines)
#                 newPixels.extend(alone)
#
#                 line = []
#                 lines = []
#                 alone = []
#
#                 lineStart = index
#
#             else:
#                 if lPixelPos.x + 1 == pos.x and lPixelPos.y == pos.y:
#                     line.append(pixels[index])
#                 elif lPixelPos.y + 1 == pos.y and lPixelPos.x == pos.x:
#                     line.append(pixels[index])
#                 else:
#                     lineEnd = lInd
#                     if lineStart == lineEnd and lineStart != None:
#                         alone.append(pixels[lineStart])
#                     else:
#                         lines.extend(line)
#                         line = []
#                     lineStart = index
#
#         else:
#             lineStart = index
#
#         lPixel = color
#         lPixelPos = pos
#         lInd = index
#
#     if lineStart == lInd and lineStart != None:
#         alone.append(pixels[lineStart])
#     else:
#         lines.extend(line)
#
#     newPixels.extend(lines)
#     newPixels.extend(alone)
#
#     return newPixels

def getMouseCoor():
    return pyautogui.position()

def setMouseCoor(x,y):
    pass

def mouseIsPressed():
    pass

def grabImage(x1,y1,x2,y2):
    image = ImageGrab.grab(bbox=(x1,y1,x2,y2))
    return image

def convertImage(image, accu):
    img = image.convert('P', palette=Image.ADAPTIVE, colors=accu)
    img = img.convert('RGB', palette=Image.ADAPTIVE, colors=accu)
    return img

def click(x,y):
    pyautogui.click(x,y)

def pressKey(keys):
    pyautogui.press(keys)

def writePixel(start , x, y):
    posX = int(start[0]) + x
    posY = int(start[1]) + y
    click(posX,posY)

def linePixels(start, startLine, endLine):
    slx = startLine.x +start[0]
    sly = startLine.y +start[1]

    elx = endLine.x +start[0]
    ely = endLine.y +start[1]

    pyautogui.moveTo(slx,sly)
    pyautogui.dragTo(elx,ely)


def interupt():
    return keyPressed('esc')

def keyPressed(key):
    return keyboard.is_pressed(key)

def oneSelection():
    if keyPressed("ctrl"):
        return getMouseCoor()
    else:
        return None



def loadConfig():
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config

def updateConfig(aConfig):
    print(aConfig)
    dz = aConfig.get("settings","drawingzone")
    cp = aConfig.get("settings","colorpicker")
    r = aConfig.get("settings","r")
    g = aConfig.get("settings","g")
    b = aConfig.get("settings","b")
    p = aConfig.get("settings","pen")
    ps = aConfig.get("settings","pensize")
    f = aConfig.get("settings","finish")
    sz = aConfig.get("settings","safezone")

    config = configparser.ConfigParser()
    config.add_section('settings')
    config.set('settings', 'drawingZone', str(dz))
    config.set('settings', 'colorPicker', str(cp))
    config.set('settings', 'R', str(r))
    config.set('settings', 'G', str(g))
    config.set('settings', 'B', str(b))
    config.set('settings', 'pen', str(p))
    config.set('settings', 'penSize', str(ps))
    config.set('settings', 'finish', str(f))
    config.set('settings', 'safezone', str(sz))


    with open('config.ini', 'w') as configfile:
        config.write(configfile)

if __name__ == '__main__':
    pressKey("backspace")
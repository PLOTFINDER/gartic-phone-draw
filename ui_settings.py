# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'settingsWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_settingsWindow(object):
    def setupUi(self, settingsWindow):
        if not settingsWindow.objectName():
            settingsWindow.setObjectName(u"settingsWindow")
        settingsWindow.resize(400, 300)
        settingsWindow.setMinimumSize(QSize(400, 300))
        settingsWindow.setMaximumSize(QSize(400, 300))
        self.layoutWidget = QWidget(settingsWindow)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(40, 20, 322, 268))
        self.formLayout = QFormLayout(self.layoutWidget)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.drawingZoneButton = QPushButton(self.layoutWidget)
        self.drawingZoneButton.setObjectName(u"drawingZoneButton")

        self.formLayout.setWidget(0, QFormLayout.SpanningRole, self.drawingZoneButton)

        self.colorPickerButton = QPushButton(self.layoutWidget)
        self.colorPickerButton.setObjectName(u"colorPickerButton")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.colorPickerButton)

        self.penSizeButton = QPushButton(self.layoutWidget)
        self.penSizeButton.setObjectName(u"penSizeButton")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.penSizeButton)

        self.rButton = QPushButton(self.layoutWidget)
        self.rButton.setObjectName(u"rButton")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.rButton)

        self.penButton = QPushButton(self.layoutWidget)
        self.penButton.setObjectName(u"penButton")

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.penButton)

        self.gButton = QPushButton(self.layoutWidget)
        self.gButton.setObjectName(u"gButton")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.gButton)

        self.finishButton = QPushButton(self.layoutWidget)
        self.finishButton.setObjectName(u"finishButton")

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.finishButton)

        self.bButton = QPushButton(self.layoutWidget)
        self.bButton.setObjectName(u"bButton")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.bButton)

        self.safeZoneButton = QPushButton(self.layoutWidget)
        self.safeZoneButton.setObjectName(u"safeZoneButton")

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.safeZoneButton)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")

        self.formLayout.setLayout(6, QFormLayout.LabelRole, self.verticalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.importButton = QPushButton(self.layoutWidget)
        self.importButton.setObjectName(u"importButton")

        self.horizontalLayout_2.addWidget(self.importButton)

        self.exportButton = QPushButton(self.layoutWidget)
        self.exportButton.setObjectName(u"exportButton")

        self.horizontalLayout_2.addWidget(self.exportButton)

        self.saveButton = QPushButton(self.layoutWidget)
        self.saveButton.setObjectName(u"saveButton")

        self.horizontalLayout_2.addWidget(self.saveButton)

        self.settingsExitButton = QPushButton(self.layoutWidget)
        self.settingsExitButton.setObjectName(u"settingsExitButton")

        self.horizontalLayout_2.addWidget(self.settingsExitButton)


        self.formLayout.setLayout(9, QFormLayout.SpanningRole, self.horizontalLayout_2)


        self.retranslateUi(settingsWindow)

        QMetaObject.connectSlotsByName(settingsWindow)
    # setupUi

    def retranslateUi(self, settingsWindow):
        settingsWindow.setWindowTitle(QCoreApplication.translate("settingsWindow", u"Settings", None))
        self.drawingZoneButton.setText(QCoreApplication.translate("settingsWindow", u"Drawing Zone", None))
        self.colorPickerButton.setText(QCoreApplication.translate("settingsWindow", u"Color Picker", None))
        self.penSizeButton.setText(QCoreApplication.translate("settingsWindow", u"Pen size", None))
        self.rButton.setText(QCoreApplication.translate("settingsWindow", u"R", None))
        self.penButton.setText(QCoreApplication.translate("settingsWindow", u"Pen", None))
        self.gButton.setText(QCoreApplication.translate("settingsWindow", u"G", None))
        self.finishButton.setText(QCoreApplication.translate("settingsWindow", u"Finish Button", None))
        self.bButton.setText(QCoreApplication.translate("settingsWindow", u"B", None))
        self.safeZoneButton.setText(QCoreApplication.translate("settingsWindow", u"Safe Zone", None))
        self.importButton.setText(QCoreApplication.translate("settingsWindow", u"Import", None))
        self.exportButton.setText(QCoreApplication.translate("settingsWindow", u"Export", None))
        self.saveButton.setText(QCoreApplication.translate("settingsWindow", u"Save", None))
        self.settingsExitButton.setText(QCoreApplication.translate("settingsWindow", u"Exit", None))
    # retranslateUi


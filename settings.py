from PySide6.QtWidgets import QMainWindow

from ui_settings import Ui_settingsWindow

class WindowSettings(QMainWindow):
      def __init__(self):
        super(WindowSettings, self).__init__()

        # ici on associe le .ui modélisé avec le designer
        self.settingsWindow = Ui_settingsWindow()
        self.settingsWindow.setupUi(self)
